package com.mxx.caffeine.cache;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import java.util.concurrent.TimeUnit;

/**
 * Description: 封装缓存，便于以后替换成其他的缓存方案 .<br>
 *
 * @author m-xy
 *     Created By 2019/8/22 上午11:19
 */
public class TicketCache {

    /**
     * 多样配置，可以参考官网文档
     */
    private static Cache<String, Ticket> ticketCache = Caffeine.newBuilder()
        .maximumSize(10_000)
        .expireAfterWrite(1, TimeUnit.DAYS)
        .build();

    public void put(String ticketNo, Ticket ticket) {
        ticketCache.put(ticketNo, ticket);
    }

    public Ticket get(String ticketNo) {
        return ticketCache.getIfPresent(ticketNo);
    }
}
