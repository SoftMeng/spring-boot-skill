package com.mxx.caffeine.cache;

import lombok.Data;

import java.util.Date;

/**
 * Description: $VAL$ .<br>
 *
 * @author m-xy
 *     Created By 2019/8/22 上午11:18
 */
@Data
public class Ticket {
    private String ticketNo;
    private String ticketDesc;
    private int ticketPrice;
    private Date createTime;
}
