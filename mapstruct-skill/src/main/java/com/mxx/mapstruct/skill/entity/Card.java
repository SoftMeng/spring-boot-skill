package com.mxx.mapstruct.skill.entity;

import lombok.Data;

import java.util.Date;

/**
 * Description: $VAL$ .<br>
 *
 * @author m-xy
 *     Created By 2019/8/23 下午5:48
 */
@Data
public class Card {

    private String name;
    private String note;
    private String image;
    private String make;
    private Date createTime;
    private int price;
}
