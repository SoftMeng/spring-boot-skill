package com.mxx.mapstruct.skill.dto;

import lombok.Data;

import java.util.Date;

/**
 * Description: $VAL$ .<br>
 *
 * @author m-xy
 *     Created By 2019/8/23 下午5:48
 */
@Data
public class CardDto {

    private String name;
    private String note;
    private String image;
    private String maker;
    private Date createTime;
    private long price;
}
