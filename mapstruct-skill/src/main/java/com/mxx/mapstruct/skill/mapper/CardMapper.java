package com.mxx.mapstruct.skill.mapper;

import com.mxx.mapstruct.skill.dto.CardDto;
import com.mxx.mapstruct.skill.entity.Card;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * Description: $VAL$ .<br>
 *
 * @author m-xy
 *     Created By 2019/8/23 下午5:51
 */
@Mapper
public interface CardMapper {
    CardMapper INSTANCE = Mappers.getMapper(CardMapper.class);

    /**
     * card 转 CardDto .
     *
     * @param card
     * @return
     */
    @Mapping(target = "maker", source = "make")
    CardDto dto(Card card);

    /**
     * CardDt 转 card .
     *
     * @param cardDto
     * @return
     */
    @Mapping(target = "make", source = "maker")
    Card entity(CardDto cardDto);
}
