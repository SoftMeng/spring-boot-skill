<p align="center">
    <a href="http://www.jiaomatech.com/" target="_blank">
        <img src="https://images.gitee.com/uploads/images/2019/0821/150840_bf64cf9b_127263.png" width="240px"/>
    </a>
</p>


<p align="center">
  <a href="http://www.jiaomatech.com/"><img src="https://img.shields.io/badge/官网-web-blueviolet.svg" alt="官网"></a>
  <a href="#项目介绍"><img src="https://img.shields.io/badge/技巧-line-critical.svg" alt="技巧"></a>
  <a href="./LICENSE"><img src="https://img.shields.io/hexpm/l/plug.svg" alt="LICENSE"></a>
  <a href="#邮箱"><img src="https://img.shields.io/badge/邮箱-email-brightgreen.svg" alt="邮箱"></a>
  <a href="#公众号"><img src="https://img.shields.io/badge/公众号-weixin-green.svg" alt="公众号"></a>
</p>

## 目录
- [项目介绍](#项目介绍)
- [技巧](#技巧)
- [我的文章](#我的文章)
- [参考](#参考)
- [联系我们](#联系我们)
    - [邮箱](#邮箱)
    - [公众号](#公众号)
- [LICENSE](./LICENSE)

## 项目介绍

在`Spring boot`使用过程中的一些小技巧，包含了性能、优化、编码等等
    

|模块|介绍|备注|
|:--------:|:-----|:----|
|doc|doc文档目录|存放文档|
|redis-serializer-line|Redis序列化|各个Redis序列化方案的性能|
|lombok-skill|lombok的使用|lombok的使用|

## 技巧

### [Redis序列化方案选择](redis-serializer-line/README.md)
> FST >= Kryo > FastJson > JDK
从结果上看，简单项目建议直接使用`JSON`作为序列化方案，如果对性能要求比较高，那么可以使用`FST`，毕竟从实现上看比较简单。
- `Kryo`使用的是标准版本的扩展，与`Dubbo`使用的版本一致，需要通过`register`来提升性能以及线程不安全的特性，那么在实现上就比较繁琐了。
- `FastJson`在提供了`RedisSerializer`的实现，使用时只需要配置一下即可。
![Redis序列化方案耗时](https://images.gitee.com/uploads/images/2019/0821/154425_6c87b47b_127263.png "Redis序列化.png")

项目见 [redis-serializer-line](./redis-serializer-line)

### [Lombox让代码更简洁](lombok-skill/README.md)
在我这，`lombox`最大的用处就是简洁代码，我不会过度使用它，给它的使用限定了具体的范围。它也永远不会出现在我的业务代码当中。

项目见 [lombok-skill](./lombok-skill)

### [高性能本地缓存](./caffeine-skill/README.md)
经常用的高性能本地缓存有两种，`Guava`和`Caffeine`。spring boot已经放弃`Guava`，拥抱`Caffeine`。从各大网站的基准测试上可以看出，`Caffeine`有很大的性能优势。

- 原生JAVA:线程不安全，需要额外加锁，功能结构单一，没有过期时间容易存在内存泄露。
- Guava:高性能，惰性删除的策略，定时回收周期性地在写操作中执行，偶尔在读操作中执行。
- Caffeine:高性能缓存，命中率，读写性能上都比Guava Cache好很多，并且其API和Guava cache基本一致。
- Ehcache: jar包很大，较重量级。

市面上有很多缓存方案，大多是各自吹着自己的牛逼，至于效果如何，还得看疗效。其实用何种缓存并不重要，我更关注当前场景下是否应该加入本地缓存，抑或是使用`Redis`之类的分布式缓存更加合理一些。以下举例说明：

- 计算大量占用CPU:比如实时价格计算，正则表达式匹配结果等等有范围的复杂计算会占用大量的CPU，这时候可以考虑是否加入缓存，减轻CPU的压力。
- IO占用:数据库频繁调用，导致IO或者连接池很繁忙，这是可以考虑使用缓存，减少对数据库的调用，在分布式中可以选择`Redis`之类的分布式缓存方案。如果是单体服务，可以选择进程内缓存（本地缓存）。同理，一些针对第三方接口的调用也是如此，比如钉钉免密登陆使用的`access_token`等。
- 其他情况:均衡的考虑缓存带来的好处和风险，要平衡，要协调。

##### 6个线程同时读取，2个线程写入配置为最大大小的缓存
![6个线程同时读取，2个线程写入](https://images.gitee.com/uploads/images/2019/0822/105422_ca6518f8_127263.png "7525.png")
##### 8个线程同时从配置了最大大小的缓存中读取
![8个线程同时读取](https://images.gitee.com/uploads/images/2019/0822/105618_ebcbb106_127263.png "read.png")
##### 8个线程同时写入配置了最大大小的缓存
![8个线程同时写入](https://images.gitee.com/uploads/images/2019/0822/105637_07b832e6_127263.png "write.png")


效能测试来源于 [Caffeine效能](https://github.com/ben-manes/caffeine/wiki/Efficiency)
基准测试来源于 [Caffeine基准](https://github.com/ben-manes/caffeine/wiki/Benchmarks)
项目见 [caffeine-skill](./caffeine-skill)

### `Jave Bean`快速拷贝`MapStruct`
> MapStruct是一个用于生成类型安全，高性能和无依赖的bean映射代码的注释处理器。竞争对手是`Selma`。
项目及基本使用方法见 [caffeine-skill](./caffeine-skill)

生成的实现类如下图
![位置](https://images.gitee.com/uploads/images/2019/0823/175726_b5b009be_127263.png "屏幕截图.png")

## 我的文章
> 欢迎关注我的[掘金](https://juejin.im/user/5a31e8da6fb9a0450809ab85/posts)账号
- [Spring Boot 自定义注解进行参数校验](https://juejin.im/post/5d428bdae51d456201486db7)
- [微服务之唯一ID生成策略](https://juejin.im/post/5d42756ee51d4561bf46201a)
- [SpringBoot之Maven常用插件](https://juejin.im/post/5cf88319f265da1b5e72e3e0)
- [Nginx配置支持Gzip压缩](https://juejin.im/post/5cf87f8df265da1baf7cdbea)
- [【案发现场】JavaCV 下 CPU 飙升到 260%](https://juejin.im/post/5cd0f02bf265da03594887d8)
- [请给Sprint Boot多一些内存](https://juejin.im/post/5c89f266f265da2d8763b5f9)
- [SpringBoot是如何动起来的](https://juejin.im/post/5c6f730ce51d457f14363a53)
- [SpringBoot 之 自定义横幅](https://juejin.im/post/5c529b1ee51d45520d302cfe)
- [SpringBoot 之 配置数据源大全](https://juejin.im/post/5c529a0f6fb9a049f57191e5)
- [Mac笔记-我亲爱的Bear](https://juejin.im/post/5c5296d6f265da2d8d69aea3)
- [JAVA 之 我们都要熟悉的基础功能类库](https://juejin.im/post/5c4ff820e51d452a167beb1f)
- [JAVA虚拟机之堆栈异常](https://juejin.im/post/5a38b22df265da431d3cccaf)
- [JAVA编码中养成的坏习惯](https://juejin.im/post/5a31ea4b51882554bd511114)

## 参考

- [spring boot 轻量级为服务开发框架](https://spring.io/projects/spring-boot)
- [vert.x 基于JVM、轻量级、高性能的应用平台](https://vertx.io/)
- [undertow 灵活的高性能Web服务器](https://github.com/undertow-io/undertow)
- [caffeine 高性能Java 缓存库](https://github.com/ben-manes/caffeine)
- [swagger 在线API文档管理](https://swagger.io/)
- [mysql 关系型数据库](https://www.mysql.com/cn/)
- [liquibase 数据库追踪、管理、迁移工具](https://www.liquibase.org/)

## 联系我们

### 邮箱

<mengxiangyuan@jiaomatech.com>

坐标: 辽宁·大连

### 公众号

如果大家想要实时关注我们公司更新的文章以及分享的干货的话，可以关注我们公司的公众号。

![公司公众号](https://images.gitee.com/uploads/images/2019/0821/150807_37727ef2_127263.jpeg "角马二维码.jpg")



















