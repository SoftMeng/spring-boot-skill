package com.mxx.redis.serializer.line.dto;

import lombok.Data;
import org.nustaq.serialization.annotations.Version;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * Description: $VAL$ .<br>
 *
 * @author m-xy
 *     Created By 2019/8/20 下午4:12
 */
@Data
public class Book implements Serializable {
    private String name;
    private int number;
    private long version;
    private Date createTime;
    private LocalDateTime updateTime;
    private List<String> list;
    /**
     * FST 可以增加版本 Version 对新增加的字段进行管理，这样反序列化就不会报错了
     * 付出性能的成本。
     */
    @Version(1)
    private Date empty;
}
