# Lombox建议

通过简单注解精简代码达到消除冗长代码的目的。Lombok是一个实现了"JSR 269 API"的程序而已。

## 优点

1. 提高编码效率
2. 使代码更加简洁
3. 消除冗长代码
4. 避免修改字段名字时忘记修改方法名
5. 提高代码”逼格”
> 注意IDE上必须要支持Lombox,否者IDE会报错。

## 缺点

1. 需要安装IDEA的lombox插件
2. 过度使用会影响代码的可读性

## 使用策略

1. 一般用于POJO、VO、DTO、Entity等等，虽然lombox提供了很多注解，但不要过度使用
2. 正常业务代码中尽量不要使用lombox注解

## maven引入

`pom.xml`中引入
```xml
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.6</version>
            <scope>provided</scope>
        </dependency>
```
build时增加`annotationProcessorPaths`
```xml
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>3.8.0</version>
            <configuration>
                <source>1.8</source>
                <target>1.8</target>
                <encoding>UTF-8</encoding>
                <annotationProcessorPaths>
                    <path>
                        <groupId>org.projectlombok</groupId>
                        <artifactId>lombok</artifactId>
                        <version>1.18.6</version>
                    </path>
                </annotationProcessorPaths>
            </configuration>
        </plugin>
```