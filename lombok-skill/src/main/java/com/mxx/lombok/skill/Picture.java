package com.mxx.lombok.skill;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Description: $VAL$ .<br>
 *
 * @author m-xy
 *     Created By 2019/8/21 下午4:01
 */
@Data
@NoArgsConstructor
@ToString(exclude = {"size"})
public class Picture {

    private String name;

    private String url;

    private long size;
}
